package com.example.gelso.login;

import android.content.Intent;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    Button btn_get,btn_post;
    EditText username,password;
    TextView responseLabel;
    String response="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_get = (Button) findViewById(R.id.btn_get);
        btn_post = (Button) findViewById(R.id.btn_post);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        responseLabel = (TextView) findViewById(R.id.responseLabel);



        btn_get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 new SendDataGET().execute(username.getText().toString(),password.getText().toString());

            }
        });

        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SendDataPOST().execute(username.getText().toString(),password.getText().toString());
            }
        });

    }

    private class SendDataGET extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg) {
            String x = Utils.doGet(arg[0],arg[1]);
            return x;
        }

        @Override
        protected void onPostExecute(String retString) {
            super.onPostExecute(retString);

            try {

                JSONArray json = new JSONArray(retString);
                JSONObject x = json.getJSONObject(0);
                String sessionid = String.valueOf(x.getString("sessionId"));
                responseLabel.setText(sessionid);
                responseLabel.setTextColor(Color.BLACK);

            }catch (Exception e) {
                Log.d("utility",e.getMessage());
            }

        }
    }


    private class SendDataPOST extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg) {
            String x = Utils.doPost(arg[0],arg[1]);
            return x;
        }

        protected void onPostExecute(String retString) {
            super.onPostExecute(retString);

            try {


                JSONArray json = new JSONArray(retString);
                JSONObject x = json.getJSONObject(0);
                String sessionid = String.valueOf(x.getString("sessionId"));
                responseLabel.setText(sessionid);
                responseLabel.setTextColor(Color.BLACK);

            }catch (Exception e) {
                Log.d("utility",e.getMessage());
            }
        }
    }
}
